## 1.1.0 - 2019-01-16
* upgrade to 1.7.7
* import from Github
* upgrade to Alpine 3.8.2
* add gitlab-ci

### 1.0.1 - 2018-02-19
* upgrade to 1.5.2
* Tested on Raspberry Pi 1 Model B Rev 2 with Docker v18.02.0-ce


### 1.0.0 - 2017-12-08
* first release
* traefik 1.5.0-rc2
* Custom base image: Alpine Linux arm32v6 ver 3.7 with qemu-arm-static
* Tested on Raspberry Pi 1 Model B Rev 2 with  Raspbian 4.9.51-1+rpi3 (2017-10-22) armv6l GNU/Linux and Docker v17.10.0-ce (It looks Docker 17.11.0 has the problem. sudo apt install docker-ce=17.10.0~ce-0~raspbian)
